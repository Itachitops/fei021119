package br.edu.fei.sortingalgorithmvisualization.algorithms.impl;

import java.util.Arrays;

import org.jfree.chart.ChartPanel;

import br.edu.fei.sortingalgorithmvisualization.algorithms.Algorithm;
import br.edu.fei.sortingalgorithmvisualization.utils.ArrayUtils;
import br.edu.fei.sortingalgorithmvisualization.utils.ChartUtils;

public class IntroSort implements Algorithm
{
	int i = 0;
	
	public void sort(int[] arrayToSort, ChartPanel chartPanel) throws Exception
	{		
		int depth = ((int) Math.log(arrayToSort.length))*2;
		sort(arrayToSort, depth, 0, arrayToSort.length-1, chartPanel);
	}
	
	private void sort(int[] arrayToSort, int depth, int start, int end, final ChartPanel chartPanel) throws Exception
	{
		int length = arrayToSort.length;
		
		if(length <= 1){
			return;
		}else if(depth == 0){
			this.heapSort(arrayToSort, start, end, chartPanel);
		}else{
			if(start >= end)
				return;
			int pivot = arrayToSort[(start + end)/2];
			int index =  partition(arrayToSort, start, end, pivot);
			
	        ChartUtils.updateDataSet(arrayToSort, chartPanel);
	        Thread.sleep(50);
			sort(arrayToSort, depth-1, start, index-1, chartPanel);
			sort(arrayToSort, depth-1, index, end, chartPanel);
		}
	}
	
	private void heapSort(int[] arrayToSort, int start, int end, ChartPanel chartPanel) throws Exception
	{
		final int size = arrayToSort.length;

        for(int i = size / 2 - 1; i >= 0; i--)
        {
        	heapify(arrayToSort, size, i, chartPanel);
        }

        for(int i = size - 1; i >= 0; i--)
        {
            final int x = arrayToSort[0];
            arrayToSort[0] = arrayToSort[i];
            arrayToSort[i] = x;
            
            heapify(arrayToSort, i, 0, chartPanel);
        }
	}
	
	private void heapify(final int array[], final int heapSize, int i, final ChartPanel chartPanel) throws Exception
    {
    	int largest = i;
        final int leftChildIdx = 2 * i + 1;
        final int rightChildIdx = 2 * i + 2;
        
        if (leftChildIdx  < heapSize && array[leftChildIdx ] > array[largest])
        {
        	largest = leftChildIdx ;
        }
        
        if (rightChildIdx  < heapSize && array[rightChildIdx ] > array[largest])
        {
        	largest = rightChildIdx ;
        }

        if (largest != i)
        {
            final int swap = array[i];
            array[i] = array[largest];
            array[largest] = swap;

            heapify(array, heapSize, largest, chartPanel);
        }
        
		/*
		 * ChartUtils.updateDataSet(array, chartPanel); Thread.sleep(50);
		 */
    }

	private int partition(int[] arrayToSort, int start, int end, int pivot)
	{
		while(start <= end){
			while(arrayToSort[start] < pivot){
				start++;
			}
			while(arrayToSort[end] > pivot){
				end--;
			}
			if(start <= end){
				int temp = arrayToSort[start];
				arrayToSort[start] = arrayToSort[end];
				arrayToSort[end] = temp;
				start++;
				end--;
			}
		}
		return start;
	}
	
	public static void main(String[] args) throws Exception {
		final int[] arrayR = ArrayUtils.generateRandomArray(27);
		
		final IntroSort is = new IntroSort();
		is.sort(arrayR, null);
		
		System.out.println(Arrays.toString(arrayR));
	}
}