package br.edu.fei.sortingalgorithmvisualization.algorithms.impl;

import org.jfree.chart.ChartPanel;

import br.edu.fei.sortingalgorithmvisualization.algorithms.Algorithm;
import br.edu.fei.sortingalgorithmvisualization.utils.ChartUtils;

public final class SelectionSort implements Algorithm
{
	@Override
	public void sort(int[] array, ChartPanel chartPanel) throws Exception
	{
		for (int fixedValue = 0; fixedValue < array.length - 1; fixedValue++)
		{
			int min = fixedValue;
			
			for (int i = min + 1; i < array.length; i++)
			{
				if (array[i] < array[min])
				{
				min = i;
				}
			}
			if (min != fixedValue)
			{
				int t = array[fixedValue];
				array[fixedValue] = array[min];
				array[min] = t;
			}
			
			ChartUtils.updateDataSet(array, chartPanel);
			Thread.sleep(200);
		}
	}
}