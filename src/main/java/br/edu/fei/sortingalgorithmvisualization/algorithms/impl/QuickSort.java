package br.edu.fei.sortingalgorithmvisualization.algorithms.impl;

import org.jfree.chart.ChartPanel;

import br.edu.fei.sortingalgorithmvisualization.algorithms.Algorithm;
import br.edu.fei.sortingalgorithmvisualization.utils.ChartUtils;

public final class QuickSort implements Algorithm
{
	@Override
	public void sort(final int[] array, final ChartPanel chartPanel) throws Exception
	{
		this.quickSort(array, 0, array.length - 1, chartPanel);
	}
	
	private void quickSort(int[] array, int start, int end, final ChartPanel chartPanel) throws Exception
	{
        if (start < end)
        {
           final int pivotPosition = split(array, start, end);
           ChartUtils.updateDataSet(array, chartPanel);
           Thread.sleep(200);
           quickSort(array, start, pivotPosition - 1, chartPanel);
           quickSort(array, pivotPosition + 1, end, chartPanel);
        }
	}

	private int split(final int[] array, final int start, int end)
	{
        int pivot = array[start];
        int i = start + 1;
        int f = end;
        while (i <= f)
        {
            if (array[i] <= pivot)
            {
        	    i++;
            }
            else if (pivot < array[f])
            {
         	   f--;
            }
            else
            {
               int temp = array[i];
               array[i] = array[f];
               array[f] = temp;
               i++;
               f--;
            }
         }
         array[start] = array[f];
         array[f] = pivot;
         return f;
	}
}