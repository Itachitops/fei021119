package br.edu.fei.sortingalgorithmvisualization.algorithms.impl;

import org.jfree.chart.ChartPanel;

import br.edu.fei.sortingalgorithmvisualization.algorithms.Algorithm;
import br.edu.fei.sortingalgorithmvisualization.utils.ChartUtils;

public final class ShellSort implements Algorithm
{
	@Override
	public void sort(final int[] array, final ChartPanel chartPanel) throws Exception
	{
		final int n = array.length;
		  
        for (int gap = n/2; gap > 0; gap /= 2) 
        { 
            for (int i = gap; i < n; i += 1) 
            { 
                int temp = array[i]; 
  
                int j; 
                for (j = i; j >= gap && array[j - gap] > temp; j -= gap)
                {
                	array[j] = array[j - gap];
                }
                
                array[j] = temp;
                
                ChartUtils.updateDataSet(array, chartPanel);
                Thread.sleep(200);
            }
        }
	}
}